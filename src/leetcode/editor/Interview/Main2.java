package leetcode.editor.Interview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/*华为面试题目：
* 明明想在学校中请一些同学一起做一项问卷调查，为了实验的客观性，他先用计算机生成了N个
* 1到1000之间的随机整数（N≤1000），对于其中重复的数字，只保留一个，把其余相同的数去掉，
* 不同的数对应着不同的学生的学号。然后再把这些数从小到大排序，按照排好的顺序去找同学做调查。
* 请你协助明明完成“去重”与“排序”的工作(同一个测试用例里可能会有多组数据(用于不同的调查)，
* 希望大家能正确处理)。

注：测试用例保证输入参数的正确性，答题者无需验证。测试用例不止一组。当没有新的输入时，
说明输入结束。*/

public class Main2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        while(scan.hasNext()){
            int n = scan.nextInt();
            int[] array = new int[n];
            for(int i=0;i<n;i++){
                array[i] = scan.nextInt();  //1.先将所有数据加入数组中
            }
            //2.对输入的数组进行排序，从而使得相同的数据处于相邻位置。
            Arrays.sort(array);

            //3.对排好序的数组中重复的数组进行选择输出，首先输出第一个数；
            //其后只要和之前的数不同，按照顺序输出即可。
            System.out.println(array[0]);
            for(int i=1;i<n;i++){
                if(array[i] != array[i-1]) {
                    System.out.println(array[i]);
                }
            }
        }
    }
}
