package leetcode.editor.Interview;

import java.util.ArrayList;
import java.util.Scanner;

/*Main2题目的自行解法*/
public class Main2_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<Integer> list = new ArrayList<>();
        int num, index;

        while (scanner.hasNext()){
            Integer[] ints = new Integer[1001]; //注意此处内部创建是为了防止上次的数据在此次出现。
            num = scanner.nextInt();
            ints[0] = num;
            for (int i = 0; i < num; i++) {
                index = scanner.nextInt();
                ints[index] = index;
            }

            for (int i = 1; i < 1001; i++) {
                if(ints[i] != null){
                    System.out.println(i);
                }
            }
        }

    }
}
