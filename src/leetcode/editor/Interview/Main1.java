package leetcode.editor.Interview;

import java.util.Scanner;

public class Main1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            int num = scanner.nextInt();
            if (num==0){
                return;
            }
            System.out.println(num/2);
        }
    }
}
