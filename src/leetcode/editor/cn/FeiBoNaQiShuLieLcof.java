package leetcode.editor.cn;

//写一个函数，输入 n ，求斐波那契（Fibonacci）数列的第 n 项（即 F(N)）。斐波那契数列的定义如下： 
//
// 
//F(0) = 0,   F(1) = 1
//F(N) = F(N - 1) + F(N - 2), 其中 N > 1. 
//
// 斐波那契数列由 0 和 1 开始，之后的斐波那契数就是由之前的两数相加而得出。 
//
// 答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。 
//
// 
//
// 示例 1： 
//
// 
//输入：n = 2
//输出：1
// 
//
// 示例 2： 
//
// 
//输入：n = 5
//输出：5
// 
//
// 
//
// 提示： 
//
// 
// 0 <= n <= 100 
// 
// Related Topics 递归 
// 👍 133 👎 0

public class FeiBoNaQiShuLieLcof{
    public static void main(String[] args) {
        Solution solution = new FeiBoNaQiShuLieLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int fib(int n) {
        //考虑使用递归：
        /*if(n<0){
            return -1;
        }
        else if(n==0){
            return 0;
        }
        else if (n==1 || n==2){
            return 1;
        } else {
            return (fib(n-1)+fib(n-2))% 1000000007;
        }*/
        if(n<0){
            return -1;
        }
        else if(n==0){
            return 0;
        }
        else if (n==1 || n==2){
            return 1;
        }

        int a=1,b=1,temp;
        for (int i = 3; i <= n; i++) {
            temp = (a+b) % 1000000007;  //应该在进入计算之前，取余，否则对于大数会产生错误
            a= b;
            b= temp;
        }
        return b ;




    }
}
//leetcode submit region end(Prohibit modification and deletion)

}