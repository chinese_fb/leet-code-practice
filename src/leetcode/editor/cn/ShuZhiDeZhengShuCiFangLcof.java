package leetcode.editor.cn;

//实现 pow(x, n) ，即计算 x 的 n 次幂函数（即，xn）。不得使用库函数，同时不需要考虑大数问题。 
//
// 
//
// 示例 1： 
//
// 
//输入：x = 2.00000, n = 10
//输出：1024.00000
// 
//
// 示例 2： 
//
// 
//输入：x = 2.10000, n = 3
//输出：9.26100 
//
// 示例 3： 
//
// 
//输入：x = 2.00000, n = -2
//输出：0.25000
//解释：2-2 = 1/22 = 1/4 = 0.25 
//
// 
//
// 提示： 
//
// 
// -100.0 < x < 100.0 
// -231 <= n <= 231-1 
// -104 <= xn <= 104 
// 
//
// 
//
// 注意：本题与主站 50 题相同：https://leetcode-cn.com/problems/powx-n/ 
// Related Topics 递归 
// 👍 151 👎 0

public class ShuZhiDeZhengShuCiFangLcof{
    public static void main(String[] args) {
        Solution solution = new ShuZhiDeZhengShuCiFangLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public double myPow(double x, int n) {
        /*暴力方式：特判情况：n为0；x为0、±1的情况；
        * 然后讨论n为正负时的情况。特别注意当值无线小，直接赋值为0.*/
        if (n==0){ return 1;}
        if (x==1||x==0){ return x;}
        if (x==-1){ return n%2==0?1:-1;}
        double res = 1;

        if(n>0){
            for (int i = 0; i < n; i++) {
                res*=x;
            }
            return res;
        }else{
            for (int i = 0; i > n; i--) {
                res*=x;
                if(1/res<1e-7){
                    return 0;
                }
            }
            return 1/res;
        }
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}