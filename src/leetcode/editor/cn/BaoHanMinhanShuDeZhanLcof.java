package leetcode.editor.cn;

//定义栈的数据结构，请在该类型中实现一个能够得到栈的最小元素的 min 函数在该栈中，调用 min、push 及 pop 的时间复杂度都是 O(1)。 
//
// 
//
// 示例: 
//
// MinStack minStack = new MinStack();
//minStack.push(-2);
//minStack.push(0);
//minStack.push(-3);
//minStack.min();   --> 返回 -3.
//minStack.pop();
//minStack.top();      --> 返回 0.
//minStack.min();   --> 返回 -2.
// 
//
// 
//
// 提示： 
//
// 
// 各函数的调用总次数不超过 20000 次 
// 
//
// 
//
// 注意：本题与主站 155 题相同：https://leetcode-cn.com/problems/min-stack/ 
// Related Topics 栈 设计 
// 👍 124 👎 0

public class BaoHanMinhanShuDeZhanLcof{
    public static void main(String[] args) {
        Solution solution = new BaoHanMinhanShuDeZhanLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
class MinStack {
        /*思路：由示例可知，涉及到查找最小值、删除节点的操作，可以考虑使用队列或者链表进行完成。*/

    private Node head;  //表示新数字入栈以后，形成的新链表头节点（并含有该链表的最小值）
    /** initialize your data structure here. */
    public MinStack() {

    }
    
    public void push(int x) {
        if(head==null){
            //head为空时，表示还没有节点，需要自己添加
            head = new Node(x,x,null);
        } else {    //若果存在head,则需要根据此次的情况更新head
            head = new Node(x,Math.min(head.min,x),head);
        }
    }
    
    public void pop() { //对于弹出操作，即是直接移动head位置
        head = head.next;
    }
    
    public int top() {  //返回栈顶元素
        return head.val;
    }
    
    public int min() {  //最小元素就包含在head节点里面
        return head.min;
    }

    private class Node {
        int val;
        int min;
        Node next;

        public Node(int val, int min, Node next) {
            this.val = val;
            this.min = min;
            this.next = next;
        }
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.min();
 */
//leetcode submit region end(Prohibit modification and deletion)
    //在此处创建一个链表类


}

