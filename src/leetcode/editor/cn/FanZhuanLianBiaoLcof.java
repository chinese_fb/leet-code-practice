package leetcode.editor.cn;

//定义一个函数，输入一个链表的头节点，反转该链表并输出反转后链表的头节点。 
//
// 
//
// 示例: 
//
// 输入: 1->2->3->4->5->NULL
//输出: 5->4->3->2->1->NULL 
//
// 
//
// 限制： 
//
// 0 <= 节点个数 <= 5000 
//
// 
//
// 注意：本题与主站 206 题相同：https://leetcode-cn.com/problems/reverse-linked-list/ 
// Related Topics 链表 
// 👍 228 👎 0

public class FanZhuanLianBiaoLcof{
    public static void main(String[] args) {
        Solution solution = new FanZhuanLianBiaoLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode reverseList(ListNode head) {
        /*思路：对于链表问题，可以考虑使用递归方式进行解决。*/
        /*递归解决方式：结束条件；每一次递归应该干的事；每次递归的返回值*/
        if(head==null || head.next == null){return head;}

        //每一次递归应该干的事：在非尾结点的情况下，继续反转直至最后。
        ListNode node = reverseList(head.next); //此处是从head.next到链表尾部元素的反转
        /*此处的操作是将未参与反转的head元素挂到已反转的结果的最后*/
        head.next.next = head;
        head.next = null;

        return node;    //每次传递可以使递归进行下去的结果
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}