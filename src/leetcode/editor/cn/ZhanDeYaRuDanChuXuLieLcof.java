package leetcode.editor.cn;

//输入两个整数序列，第一个序列表示栈的压入顺序，请判断第二个序列是否为该栈的弹出顺序。假设压入栈的所有数字均不相等。例如，序列 {1,2,3,4,5} 是某栈
//的压栈序列，序列 {4,5,3,2,1} 是该压栈序列对应的一个弹出序列，但 {4,3,5,1,2} 就不可能是该压栈序列的弹出序列。 
//
// 
//
// 示例 1： 
//
// 输入：pushed = [1,2,3,4,5], popped = [4,5,3,2,1]
//输出：true
//解释：我们可以按以下顺序执行：
//push(1), push(2), push(3), push(4), pop() -> 4,
//push(5), pop() -> 5, pop() -> 3, pop() -> 2, pop() -> 1
// 
//
// 示例 2： 
//
// 输入：pushed = [1,2,3,4,5], popped = [4,3,5,1,2]
//输出：false
//解释：1 不能在 2 之前弹出。
// 
//
// 
//
// 提示： 
//
// 
// 0 <= pushed.length == popped.length <= 1000 
// 0 <= pushed[i], popped[i] < 1000 
// pushed 是 popped 的排列。 
// 
//
// 注意：本题与主站 946 题相同：https://leetcode-cn.com/problems/validate-stack-sequences/ 
// 👍 170 👎 0

import java.util.ArrayDeque;
import java.util.Deque;

public class ZhanDeYaRuDanChuXuLieLcof{
    public static void main(String[] args) {
        Solution solution = new ZhanDeYaRuDanChuXuLieLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        /*思路：遍历两个数组，一一判断,不可行*/
        /*if(pushed==null && popped==null){return true;}
        if(pushed==null || popped==null){return false;}

        if(pushed.length != popped.length){return false;}

        int len = pushed.length;
        for (int i = 0; i < len; i++) {
            if(pushed[i] == popped[len-1-i]){
                i++;
            }
            return false;
        }
        return true;*/

        /*思路2：将压栈的元素按顺序压入以后，在弹出之前，先将栈顶元素和出栈的元素做个比较；
        * 相同，则将该元素弹出，出栈列表的指针后移；至最后时，判断*/

        //栈可以用双端队列进行模拟
        Deque<Integer> stack = new ArrayDeque<>();
        int j=0;
        for (int elem : pushed) {
            stack.push(elem);
            while (j<popped.length && !stack.isEmpty() && stack.peek()==popped[j]){
                stack.pop();
                j++;
            }
        }
        return j==popped.length;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}