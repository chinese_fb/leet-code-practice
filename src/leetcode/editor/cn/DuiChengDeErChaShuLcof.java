package leetcode.editor.cn;

//请实现一个函数，用来判断一棵二叉树是不是对称的。如果一棵二叉树和它的镜像一样，那么它是对称的。 
//
// 例如，二叉树 [1,2,2,3,4,4,3] 是对称的。 
//
// 1 
// / \ 
// 2 2 
// / \ / \ 
//3 4 4 3 
//但是下面这个 [1,2,2,null,3,null,3] 则不是镜像对称的: 
//
// 1 
// / \ 
// 2 2 
// \ \ 
// 3 3 
//
// 
//
// 示例 1： 
//
// 输入：root = [1,2,2,3,4,4,3]
//输出：true
// 
//
// 示例 2： 
//
// 输入：root = [1,2,2,null,3,null,3]
//输出：false 
//
// 
//
// 限制： 
//
// 0 <= 节点个数 <= 1000 
//
// 注意：本题与主站 101 题相同：https://leetcode-cn.com/problems/symmetric-tree/ 
// Related Topics 树 
// 👍 164 👎 0

public class DuiChengDeErChaShuLcof{
    public static void main(String[] args) {
        Solution solution = new DuiChengDeErChaShuLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public boolean isSymmetric(TreeNode root) {
        /*思路：对于验证二叉树的对称性，需要验证左子树的左节点=右子树的右节点；左子树的右节点=右子树的左节点*/
        //先进行非空的特判
        if(root==null){return true;}

        //在不为空的时候，进入到递归判断
        return helper(root.left,root.right);
    }

    boolean helper(TreeNode root1,TreeNode root2){
        /*对于二叉树的结束条件，多数是从最后的节点是否为空进行考虑*/
        //结束条件：同时为空，则返回true;若果一边为空，另一边不为空，则返回false
        if(root1 == null && root2==null){return true;}
        if(root1==null && root2!=null){return false;}
        if(root1!=null && root2==null){return false;}

        //递归层的做法：需要对根节点，及左右节点进行验证
        return root1.val==root2.val && helper(root1.left,root2.right) &&
                helper(root1.right,root2.left);
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}