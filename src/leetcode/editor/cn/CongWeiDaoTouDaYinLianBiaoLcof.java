package leetcode.editor.cn;

//输入一个链表的头节点，从尾到头反过来返回每个节点的值（用数组返回）。 
//
// 
//
// 示例 1： 
//
// 输入：head = [1,3,2]
//输出：[2,3,1] 
//
// 
//
// 限制： 
//
// 0 <= 链表长度 <= 10000 
// Related Topics 链表 
// 👍 135 👎 0

public class CongWeiDaoTouDaYinLianBiaoLcof{
    public static void main(String[] args) {
        Solution solution = new CongWeiDaoTouDaYinLianBiaoLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public int[] reversePrint(ListNode head) {
        /*注意此处仅是将节点的值进行倒序输出，因此可以考虑“节点的正序遍历+数组的倒序添加”即可。*/
         /*
        思路：题目中描述的是将节点的值反序输出即可。因此可以考虑正序遍历链表，将节点值倒序放入某数组中即可。*/
        int count=0;
        ListNode node1 = head;
        while (node1 !=null){
            count++;
            node1 = node1.next;
        }

        int[] ints = new int[count];
        ListNode node2 = head;
        for(int i=count-1; i>=0; i--){
            ints[i] = node2.val;
            node2 = node2.next;
        }

        return ints;
    }
}

    public class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }


//leetcode submit region end(Prohibit modification and deletion)

}