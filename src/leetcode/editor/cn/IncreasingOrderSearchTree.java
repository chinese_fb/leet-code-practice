package leetcode.editor.cn;

//给你一棵二叉搜索树，请你 按中序遍历 将其重新排列为一棵递增顺序搜索树，使树中最左边的节点成为树的根节点，并且每个节点没有左子节点，只有一个右子节点。 
//
// 
//
// 示例 1： 
//
// 
//输入：root = [5,3,6,2,4,null,8,1,null,null,null,7,9]
//输出：[1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]
// 
//
// 示例 2： 
//
// 
//输入：root = [5,1,7]
//输出：[1,null,5,null,7]
// 
//
// 
//
// 提示： 
//
// 
// 树中节点数的取值范围是 [1, 100] 
// 0 <= Node.val <= 1000 
// 
// Related Topics 树 深度优先搜索 递归 
// 👍 214 👎 0

public class IncreasingOrderSearchTree{
    public static void main(String[] args) {
        Solution solution = new IncreasingOrderSearchTree().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    TreeNode head = new TreeNode(0); //要输出的二叉树的头结点，用于递归中的输出传递过程
    private TreeNode resNode = head;

    public TreeNode increasingBST(TreeNode root) {
        //使用递归方式,通过中序遍历进行节点添加,然后返回resNode的右节点
        inOrderAndAdd(root);

        /*注意此处不能使用return resNode.right;
        * 因为resNode在递归过程中会逐渐移动，到最后时容易造成空指针异常*/
        return head.right;      //此处返回的结果是每一次添加后从头结点开始遍历
    }

     void inOrderAndAdd(TreeNode root){ //此处只是添加操作
         //递归截止条件
         if(root==null){return;}

        //中序遍历：左  中 右
        inOrderAndAdd(root.left);

        resNode.right = new TreeNode(root.val);
        resNode = resNode.right;    //移动resNode

         inOrderAndAdd(root.right);
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}