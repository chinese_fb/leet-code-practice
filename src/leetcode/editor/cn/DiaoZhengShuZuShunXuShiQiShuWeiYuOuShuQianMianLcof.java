package leetcode.editor.cn;

//输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有奇数位于数组的前半部分，所有偶数位于数组的后半部分。 
//
// 
//
// 示例： 
//
// 
//输入：nums = [1,2,3,4]
//输出：[1,3,2,4] 
//注：[3,1,2,4] 也是正确的答案之一。 
//
// 
//
// 提示： 
//
// 
// 0 <= nums.length <= 50000 
// 1 <= nums[i] <= 10000 
// 
// 👍 120 👎 0

import java.util.Arrays;

public class DiaoZhengShuZuShunXuShiQiShuWeiYuOuShuQianMianLcof{
    public static void main(String[] args) {
        Solution solution = new DiaoZhengShuZuShunXuShiQiShuWeiYuOuShuQianMianLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[] exchange(int[] nums) {
        /*思路1：使用头尾双指针方式，如果发现不对的数字等待交换；交换完毕后，继续前进，直至两指针相遇。
        * 思路2：使用两个数组，第一遍捡取奇数，另一边捡取偶数*/
        int[] res = new int[nums.length];
        int index = 0;

        for (int i = 0; i < nums.length; i++) {
            if(nums[i]%2==1){
                res[index++] = nums[i];
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if(nums[i]%2==0){
                res[index++] = nums[i];
            }
        }
        return res;

    }
}
//leetcode submit region end(Prohibit modification and deletion)

}