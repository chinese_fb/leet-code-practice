package leetcode.editor.cn;

//请实现一个函数按照之字形顺序打印二叉树，即第一行按照从左到右的顺序打印，第二层按照从右到左的顺序打印，第三行再按照从左到右的顺序打印，其他行以此类推。 
//
// 
//
// 例如: 
//给定二叉树: [3,9,20,null,null,15,7], 
//
//     3
//   / \
//  9  20
//    /  \
//   15   7
// 
//
// 返回其层次遍历结果： 
//
// [
//  [3],
//  [20,9],
//  [15,7]
//]
// 
//
// 
//
// 提示： 
//
// 
// 节点总数 <= 1000 
// 
// Related Topics 树 广度优先搜索 
// 👍 95 👎 0

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CongShangDaoXiaDaYinErChaShuIiiLcof{
    public static void main(String[] args) {
        Solution solution = new CongShangDaoXiaDaYinErChaShuIiiLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        /*思路：较上一题的每层打印，区别之处在于每层的结果需要根据奇偶层进行区分。*/
        LinkedList<TreeNode> queue = new LinkedList<>();
        List<List<Integer>> res = new ArrayList<>();
        if (root!=null){queue.add(root);}
        int level = 1;

        while (!queue.isEmpty()){
            ArrayList<Integer> temp = new ArrayList<>();
            /*初步获取到现有的队列长度（即该层节点的个数），并将所有节点的值加入temp*/
            for (int i = queue.size(); i > 0; i--) {
                TreeNode node = queue.poll();
                temp.add(node.val);

                if (node.left!=null){queue.add(node.left);}
                if (node.right!=null){queue.add(node.right);}
            }

            if(level %2==0){
                //对于偶数层，使用Collections中的方法先将节点首位互换后，再加入到结果数组中
                Collections.reverse(temp);
                res.add(temp);
            } else {
                res.add(temp);
            }

            //在for循环执行完一层的操作之后，需要对层计数进行更新
            level++;
        }
        return res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}