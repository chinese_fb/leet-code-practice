package leetcode.editor.cn;

//请实现一个函数用来判断字符串是否表示数值（包括整数和小数）。 
//
// 数值（按顺序）可以分成以下几个部分： 
//
// 
// 若干空格 
// 一个 小数 或者 整数 
// （可选）一个 'e' 或 'E' ，后面跟着一个 整数 
// 若干空格 
// 
//
// 小数（按顺序）可以分成以下几个部分： 
//
// 
// （可选）一个符号字符（'+' 或 '-'） 
// 下述格式之一：
// 
// 至少一位数字，后面跟着一个点 '.' 
// 至少一位数字，后面跟着一个点 '.' ，后面再跟着至少一位数字 
// 一个点 '.' ，后面跟着至少一位数字 
// 
// 
// 
//
// 整数（按顺序）可以分成以下几个部分： 
//
// 
// （可选）一个符号字符（'+' 或 '-'） 
// 至少一位数字 
// 
//
// 部分数值列举如下： 
//
// 
// ["+100", "5e2", "-123", "3.1416", "-1E-16", "0123"] 
// 
//
// 部分非数值列举如下： 
//
// 
// ["12e", "1a3.14", "1.2.3", "+-5", "12e+5.4"] 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：s = "0"
//输出：true
// 
//
// 示例 2： 
//
// 
//输入：s = "e"
//输出：false
// 
//
// 示例 3： 
//
// 
//输入：s = "."
//输出：false 
//
// 示例 4： 
//
// 
//输入：s = "    .1  "
//输出：true
// 
//
// 
//
// 提示： 
//
// 
// 1 <= s.length <= 20 
// s 仅含英文字母（大写和小写），数字（0-9），加号 '+' ，减号 '-' ，空格 ' ' 或者点 '.' 。 
// 
// Related Topics 数学 
// 👍 183 👎 0

public class BiaoShiShuZhiDeZiFuChuanLcof{
    public static void main(String[] args) {
        Solution solution = new BiaoShiShuZhiDeZiFuChuanLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public boolean isNumber(String s) {
        /*思路：标记在遍历s过程中遇到的情况。并分情况进行判断正确与否。（使用多种flag对遇到的多种字符进行判断）
        * 注意：“·”不能2次出现，且不能出现在e之后（因为此处可以理解为科学计数法，·只能在之前）；
        * “e”不能2次出现，且e之前必须存在一个数；
        * 对于正负号，只能出现在最初位置，或者e之后才合法。
        * 出现其他字符均为错误。*/
        if(s.length()==0 || s==null){
            return false;
        }

        //使用三个flag标记中间可能遇到的情况
        boolean numSeen = false;
        boolean dotSeen = false;
        boolean eSeen = false;

        /*注意此种方法只能判断i位及之前的情况，不能判断之后。*/
        char[] chars = s.trim().toCharArray();
        for (int i = 0; i < chars.length; i++) {
           if(chars[i]>='0' && chars[i]<='9'){
               numSeen = true;
           }
           else if(chars[i]=='.'){
               //对于“·”的情况，应该保证不二次出现，且之前没有出现过e;
               if(dotSeen || eSeen){
                   return false;
               }
               dotSeen = true;
           }
           else if(chars[i]=='e' || chars[i]=='E'){
               if(eSeen || !numSeen){   //此处需要注意“e”之前必须有数字
                   return false;
               }
               eSeen = true;
               numSeen = false; //此处重置为false的原因是，为了保证之后再次出现数字num，才算成功
           }
           else if(chars[i]=='+' || chars[i] =='-'){
               //正负号只能出现在首位，或e之后
               if(i!=0 && chars[i-1]!='e' && chars[i-1]!='E'){
                   return false;
               }
           }
           else if(chars[i]==' ' && i==s.length()-1){
               numSeen = true;
           }
           else {
               return false;    //排除出现其他字符的情况
           }
        }
        return numSeen;     //最终的结果由numseen而定
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}