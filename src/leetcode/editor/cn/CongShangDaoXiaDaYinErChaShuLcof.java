package leetcode.editor.cn;

//从上到下打印出二叉树的每个节点，同一层的节点按照从左到右的顺序打印。 
//
// 
//
// 例如: 
//给定二叉树: [3,9,20,null,null,15,7], 
//
//     3
//   / \
//  9  20
//    /  \
//   15   7
// 
//
// 返回： 
//
// [3,9,20,15,7]
// 
//
// 
//
// 提示： 
//
// 
// 节点总数 <= 1000 
// 
// Related Topics 树 广度优先搜索 
// 👍 84 👎 0

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class CongShangDaoXiaDaYinErChaShuLcof{
    public static void main(String[] args) {
        Solution solution = new CongShangDaoXiaDaYinErChaShuLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public int[] levelOrder(TreeNode root) {
        /*思路：易得此题是对二叉树中的节点进行层序遍历。
        * 其中使用queue队列存储遇到的节点，并将其值添加到数组中*/
        if(root==null){
            return new int[]{}; //初始化一个值为空的数组；
        }

        ArrayDeque<TreeNode> queue = new ArrayDeque<>();    //存放节点
        ArrayList<Integer> list = new ArrayList<>();  //注意此处不使用int[]的原因是还不知道规模
        queue.offer(root);

        while (!queue.isEmpty()){
            //将节点添加进来，
            TreeNode node = queue.poll();
            list.add(node.val);

            //添加左右子节点
            if(node.left!=null){
                queue.offer(node.left);
            }
            if (node.right!=null){
                queue.offer(node.right);
            }
        }

        //在结束后，将结果集合list转为int【】
        int[] res = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            res[i]= list.get(i);
        }

        return res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}