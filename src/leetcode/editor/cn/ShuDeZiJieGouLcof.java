package leetcode.editor.cn;

//输入两棵二叉树A和B，判断B是不是A的子结构。(约定空树不是任意一个树的子结构) 
//
// B是A的子结构， 即 A中有出现和B相同的结构和节点值。 
//
// 例如: 
//给定的树 A: 
//
// 3 
// / \ 
// 4 5 
// / \ 
// 1 2 
//给定的树 B： 
//
// 4 
// / 
// 1 
//返回 true，因为 B 与 A 的一个子树拥有相同的结构和节点值。 
//
// 示例 1： 
//
// 输入：A = [1,2,3], B = [3,1]
//输出：false
// 
//
// 示例 2： 
//
// 输入：A = [3,4,5,1,2], B = [4,1]
//输出：true 
//
// 限制： 
//
// 0 <= 节点个数 <= 10000 
// Related Topics 树 
// 👍 256 👎 0

public class ShuDeZiJieGouLcof{
    public static void main(String[] args) {
        Solution solution = new ShuDeZiJieGouLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public boolean isSubStructure(TreeNode A, TreeNode B) {
        /*思路：从A的根节点、左右子节点三方面判断是否是子结构。
        * 先判断根节点A与B是否相等；然后考虑使用递归方式解决左右节点相互匹配的情况*/
        if(A==null || B==null){
            return false;   //B为空不成立，因为规定空树不是一个子结构。
        }
        //对于A B均不为空的情况下，需要跟节点相同，或者在A的左右节点上找到B即可
        return aEqualB(A,B) || isSubStructure(A.left,B) || isSubStructure(A.right,B);
    }

    /*此处是通过递归方式判断两个节点是否相等（根节点、左右节点均相同才行）*/
     boolean aEqualB(TreeNode A, TreeNode B){
        /*注意判断顺序：B的空判断应该A之前，即先确定B不为空时,A为空才为错误情况。*/
        if(B==null){return true;}

        if(A==null){return false;}  //注意A B的空判断顺序。

        //当二者均不为空的情况下，需要判断该值相等、左右节点和B的一一对应关系。
         return A.val==B.val && aEqualB(A.left,B.left) && aEqualB(A.right,B.right);
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}

class TreeNode {
     int val;
     TreeNode left;
     TreeNode right;
     TreeNode(int x) { val = x; }
 }