package leetcode.editor.cn;

//输入某二叉树的前序遍历和中序遍历的结果，请重建该二叉树。假设输入的前序遍历和中序遍历的结果中都不含重复的数字。 
//
// 
//
// 例如，给出 
//
// 前序遍历 preorder = [3,9,20,15,7]
//中序遍历 inorder = [9,3,15,20,7] 
//
// 返回如下的二叉树： 
//
//     3
//   / \
//  9  20
//    /  \
//   15   7 
//
// 
//
// 限制： 
//
// 0 <= 节点个数 <= 5000 
//
// 
//
// 注意：本题与主站 105 题重复：https://leetcode-cn.com/problems/construct-binary-tree-from-
//preorder-and-inorder-traversal/ 
// Related Topics 树 递归 
// 👍 418 👎 0

import java.util.Arrays;

public class ZhongJianErChaShuLcof{
    public static void main(String[] args) {
        Solution solution = new ZhongJianErChaShuLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        /*思路：对于二叉树问题，可以考虑使用递归进行解决：在此题中，构建二叉树时，我们需要通过递归
        查找到每一个节点的根节点、左节点、右节点即可。
        对于前序遍历（根 左 右），我们可以确定所有节点的个数、根节点上的数据；
        对于中序遍历（左 根 右），我们可以根据根节点的位置，确定左右节点的个数（在递归中有用）。
        终止条件：
        每一层应该干什么：根据左右遍历的特定，确定根 左 右节点。
        返回什么值： 每一层都是根据根节点进行的，因此需要返回根节点即可。
        */
        //1.从下面的递归可以看出一切是从根节点开始的，如果确定不了根节点，则可以结束了（此时的
        // 特征正好是长度为0）。
        int n = preorder.length;
        if (n == 0){
            return null;
        }
        int rootVal = preorder[0], rootIndex = 0;
        for (int i = 0; i < n; i++) {
            if (inorder[i] == rootVal) {
                rootIndex = i;
                break;
            }
        }
        TreeNode root = new TreeNode(rootVal);
        root.left = buildTree(Arrays.copyOfRange(preorder, 1, 1 + rootIndex), Arrays.copyOfRange(inorder, 0, rootIndex));
        root.right = buildTree(Arrays.copyOfRange(preorder, 1 + rootIndex, n), Arrays.copyOfRange(inorder, rootIndex + 1, n));

        return root;
    }

    public class TreeNode {
      int val;
     TreeNode left;
     TreeNode right;
     TreeNode(int x) { val = x; }
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}