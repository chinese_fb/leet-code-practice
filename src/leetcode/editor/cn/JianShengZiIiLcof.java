package leetcode.editor.cn;

//给你一根长度为 n 的绳子，请把绳子剪成整数长度的 m 段（m、n都是整数，n>1并且m>1），每段绳子的长度记为 k[0],k[1]...k[m - 1]
// 。请问 k[0]*k[1]*...*k[m - 1] 可能的最大乘积是多少？例如，当绳子的长度是8时，我们把它剪成长度分别为2、3、3的三段，此时得到的最大乘
//积是18。 
//
// 答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。 
//
// 
//
// 示例 1： 
//
// 输入: 2
//输出: 1
//解释: 2 = 1 + 1, 1 × 1 = 1 
//
// 示例 2: 
//
// 输入: 10
//输出: 36
//解释: 10 = 3 + 3 + 4, 3 × 3 × 4 = 36 
//
// 
//
// 提示： 
//
// 
// 2 <= n <= 1000 
// 
//
// 注意：本题与主站 343 题相同：https://leetcode-cn.com/problems/integer-break/ 
// Related Topics 数学 动态规划 
// 👍 102 👎 0

public class JianShengZiIiLcof{
    public static void main(String[] args) {
        Solution solution = new JianShengZiIiLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int cuttingRope(int n) {
        /*思路：此处涉及到大数问题，因此在使用dp算法时，需要注意每一步的大数取余问题。
        *       因此可以考虑使用贪心算法（每一小步均取最优解）进行解决。*/

        /*根据分析可知：对于n=2，按照要求，结果为1；n=3时，最优结果为1*2；
        * n=4时，最优解为2*2；n=5时，最优解为2*3；n=6时，最优解为3*3;
        * 可见应该按照3进行切分，且应该注意尽量不出现1*/
        if(n < 4){
            return n - 1;
        }

        long res = 1;   //使用long类型参与计算，最后转换为int类型即可
        while (n>4) {
            res =  res*3 % 1000000007;  //将每次截取出来的3乘到res中，并且注意取余运算。
            n -= 3; //当长度>4时，循环截取(注意此处只需要截取到4，当出现<4时，不应继续截取。)
        }
        return (int) (n*res % 1000000007);

    }
}
//leetcode submit region end(Prohibit modification and deletion)

}