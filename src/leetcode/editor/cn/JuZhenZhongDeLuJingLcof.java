package leetcode.editor.cn;

//给定一个 m x n 二维字符网格 board 和一个字符串单词 word 。如果 word 存在于网格中，返回 true ；否则，返回 false 。 
//
// 单词必须按照字母顺序，通过相邻的单元格内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母不允许被重复使用。 
//
// 
//
// 例如，在下面的 3×4 的矩阵中包含单词 "ABCCED"（单词中的字母已标出）。 
//
// 
//
// 
//
// 示例 1： 
//
// 
//输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "AB
//CCED"
//输出：true
// 
//
// 示例 2： 
//
// 
//输入：board = [["a","b"],["c","d"]], word = "abcd"
//输出：false
// 
//
// 
//
// 提示： 
//
// 
// 1 <= board.length <= 200 
// 1 <= board[i].length <= 200 
// board 和 word 仅由大小写英文字母组成 
// 
//
// 
//
// 注意：本题与主站 79 题相同：https://leetcode-cn.com/problems/word-search/ 
// Related Topics 深度优先搜索 
// 👍 304 👎 0

public class JuZhenZhongDeLuJingLcof{
    public static void main(String[] args) {
        Solution solution = new JuZhenZhongDeLuJingLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public boolean exist(char[][] board, String word) {
        /*由题意可知，此题为（dfs）搜索问题。对于dfs问题，需要有一个标记访问数组visited[];序号i,j.*/
        //过程为：当首位字符已经匹配成功时，才能用dfs继续查找；若果返回结果为false,找到下一个匹配地点。


        char[] chs = word.toCharArray();
        boolean[][] isVisited = new boolean[board.length][board[0].length];

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if(board[i][j] == chs[0]){
                    //此处开始启用dfs进行查找，若果结果为true,直接返回
                    if(dfs(board,chs,i,j,0,isVisited)){return true;}
                    //如果返回结果为false,表示未能完全匹配成功，应该继续从头匹配
                }
            }
        }
        return false;
    }

    /*dfs中的变量：标记访问的数组isVisited；标记序号i,j；查找对象的起始索引index*/
    /*使用dfs完成字符串word的查找，返回true时，表示完成任务；返回false，表示未正确匹配。*/
    /*注意题目要求，如果每个字符存在，则返回true,因此dfs返回的结果为boolean值*/
    private boolean dfs(char[][] board, char[] chs,int i,int j,int index,boolean[][] isVisited) {
        /*1.结束条件：当检索完成时，正确返回；当出现其他错误情况时，结束即可。*/
        if(index == chs.length){return true;}
        if(i<0 || i==board.length || j<0 || j==board[0].length || isVisited[i][j] || chs[index]!=board[i][j]){
            return false;
        }

        //2、在可执行dfs时的程序:标记当前位为已访问；按照既定策略寻找下一位，直至结束时返回true；重新标记为未访问.
        /*此处已经含有递归回溯过程，因此可以判断出结果。*/
        isVisited[i][j] = true;
        boolean res= dfs(board,chs,i-1,j,index+1,isVisited) ||
                     dfs(board,chs,i,j+1,index+1,isVisited) ||
                     dfs(board,chs,i+1,j,index+1,isVisited) ||
                     dfs(board,chs,i,j-1,index+1,isVisited);

        /*切记在回溯过程中，应该将当前位重洗标记为未访问*/
        isVisited[i][j] = false;
        return res;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}