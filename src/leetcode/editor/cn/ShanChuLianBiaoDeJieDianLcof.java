package leetcode.editor.cn;

//给定单向链表的头指针和一个要删除的节点的值，定义一个函数删除该节点。 
//
// 返回删除后的链表的头节点。 
//
// 注意：此题对比原题有改动 
//
// 示例 1: 
//
// 输入: head = [4,5,1,9], val = 5
//输出: [4,1,9]
//解释: 给定你链表中值为 5 的第二个节点，那么在调用了你的函数之后，该链表应变为 4 -> 1 -> 9.
// 
//
// 示例 2: 
//
// 输入: head = [4,5,1,9], val = 1
//输出: [4,5,9]
//解释: 给定你链表中值为 1 的第三个节点，那么在调用了你的函数之后，该链表应变为 4 -> 5 -> 9.
// 
//
// 
//
// 说明： 
//
// 
// 题目保证链表中节点的值互不相同 
// 若使用 C 或 C++ 语言，你不需要 free 或 delete 被删除的节点 
// 
// Related Topics 链表 
// 👍 123 👎 0

public class ShanChuLianBiaoDeJieDianLcof{
    public static void main(String[] args) {
        Solution solution = new ShanChuLianBiaoDeJieDianLcof().new Solution();}

//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode deleteNode(ListNode head, int val) {
//        思路：对于链表问题，可以考虑使用递归方式进行解决。
        /*结束条件*/
      //  if(head==null){return null;}
       // if(head.val==val){return head.next;}

        /*每一层应该干什么：对于该题来说，使用该方法进行删除，然后返回已删除后，留下的头结点。
        * 此处是从head.next进行递归删除（因为head的情况已经处理），并将返回结果给head.next（
        * 因为此处是递归条件，因此head.next肯定是删除后留下的头结点，否则递归结束。）*/
       // head.next = deleteNode(head.next, val);

        /*返回什么内容*/
       // return head;    //每一层传递的递归条件：head

        if(head==null){return null;}
        if (head.val==val){return head.next;}

        ListNode temp = head;
        while (temp.next.val!=val){
            temp = temp.next;
            if(temp.next==null){return head;}
        }
        temp.next = temp.next.next;

        return head;
    }
}

//leetcode submit region end(Prohibit modification and deletion)

}

class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
}
